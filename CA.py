import cfg
import base64
from OpenSSL import crypto

from tempfile import NamedTemporaryFile, TemporaryFile
from flask import Flask, request, jsonify

app = Flask(__name__)

app.config['MAX_CONTENT_LENGTH'] = 1 * 1024 * 1024

with open(cfg.CA_PRIVATE_KEY, 'r') as key:
    ca_key = key.read()
ca_key = crypto.load_privatekey(
    crypto.FILETYPE_PEM, ca_key, cfg.CA_PASS.encode())


@app.post("/sign")
def hello_world():
    try:
        f = request.files['data']
        with NamedTemporaryFile() as saveFile:
            f.save(saveFile.name)
            saveFile.seek(0)
            data = saveFile.read()
            sign = crypto.sign(ca_key, data, 'sha512')

            return base64.b64encode(sign)
    except:
        return b'bad request', 400

if __name__ == '__main__':
    # FIXME: should be localhost on production
    # app.run(host='127.0.0.1', port=7227)
    app.run(host='0.0.0.0', port=7227)
