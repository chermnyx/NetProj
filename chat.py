import traceback
from io import BytesIO
import sys
import os
from typing import Optional, TextIO

import OpenSSL
import cfg
import requests
import socket
import socketserver
import time
import uuid
import base64
import json
import binascii
import netifaces
import ipaddress
import requests
import secrets

from dataclasses import dataclass
from OpenSSL import crypto
from threading import Thread, Lock, current_thread
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import hashes

with open(cfg.CA_PUBLIC_KEY) as f:
    ca_pub = f.read()
    ca = crypto.load_certificate(crypto.FILETYPE_PEM, ca_pub)

keypair = crypto.PKey()
keypair.generate_key(crypto.TYPE_RSA, 4096)

key_b = crypto.dump_privatekey(crypto.FILETYPE_PEM, keypair)
key = crypto.load_privatekey(crypto.FILETYPE_PEM, key_b)

out_lock = Lock()

cert = crypto.X509()
cert.gmtime_adj_notBefore(0)
cert.gmtime_adj_notAfter(60 * 60 * 24 * 365 * 10)  # ~ 10 years
cert.get_subject().C = 'RU'
cert.get_subject().ST = 'Innopolis'
cert.get_subject().O = 'Innopolis University'
cert.get_subject().OU = 'Innopolis University'
cert.get_subject().CN = 'innopolis.university'
cert.set_issuer(cert.get_subject())

cert.set_pubkey(keypair)
cert.sign(keypair, 'sha512')

cert_b = crypto.dump_certificate(crypto.FILETYPE_PEM, cert)
with requests.post(cfg.CA_URL, files={'data': ('cert.pem', cert_b)}) as resp:
    cert_sign = base64.b64decode(resp.text)

key_cr = serialization.load_pem_private_key(key_b, password=None)


def enc(msg, pubkey):
    return base64.b64encode(pubkey.encrypt(msg, padding.OAEP(
        mgf=padding.MGF1(algorithm=hashes.SHA512()),
        algorithm=hashes.SHA512(),
        label=None
    )))


def dec(ciphertext, privkey):
    return privkey.decrypt(base64.b64decode(ciphertext), padding.OAEP(
        mgf=padding.MGF1(algorithm=hashes.SHA512()),
        algorithm=hashes.SHA512(),
        label=None
    ))


peer_lock = Lock()
peers: 'dict[bytes, Peer]' = {
    # digset -> Peer
}

peer_addrs = set()

broadcast_addrs = [
    str(ipaddress.IPv4Network((
        x['addr'],
        x['netmask']
    ), strict=False).broadcast_address)
    for ifname in netifaces.interfaces()
    if len(y := netifaces.ifaddresses(ifname).get(netifaces.AF_INET, [])) and (x := y[0])
]

local_addresses = {
    x['addr']
    for ifname in netifaces.interfaces()
    if len(y := netifaces.ifaddresses(ifname).get(netifaces.AF_INET, [])) and (x := y[0])
}

self_digset = cert.digest('sha512')


def print_(*args, **kwargs):
    with out_lock:
        print(end='\r')
        print(*args, **kwargs)
        print('> ', end='', flush=True)


@dataclass
class Peer:
    addr: str
    cert: crypto.X509
    socket: socket.socket
    fsock: BytesIO
    chat_worker: Thread
    ping_misses: int = 0
    last_messsage_id = 0
    lock = Lock()
    alive = True

    @property
    def digset(self) -> bytes:
        return self.cert.digest('sha512')

    @property
    def cert_cr(self):
        pubkey = self.cert.get_pubkey()
        pubkey_b = crypto.dump_publickey(crypto.FILETYPE_PEM, pubkey)
        return serialization.load_pem_public_key(pubkey_b)

    def send_msg(self, msg: dict):
        id_ = self.last_messsage_id

        msg['id'] = id_ + 1
        msg_b = json.dumps(msg).encode('utf-8')
        msg_c = enc(msg_b, self.cert_cr)
        msg_sign = base64.b64encode(crypto.sign(key, msg_c, 'sha512'))
        msg_w_digset = msg_c \
            + b'&&&' + msg_sign \
            + b'\n'

        try:
            self.socket.send(msg_w_digset)
        except Exception:
            self.alive = False

        self.last_messsage_id += 1


def handle_messages(in_peer: Peer):
    try:
        in_peer.socket.settimeout(None)
        while True:
            line = in_peer.fsock.readline()
            if not line:
                continue
            msg_c, sign = line.strip().split(b'&&&')
            sign = base64.b64decode(sign)

            crypto.verify(in_peer.cert, sign, msg_c, 'sha512')

            digset = in_peer.digset
            if digset == self_digset:
                # ignore own messages
                continue
            msg = json.loads(dec(msg_c, key_cr))

            with peer_lock:
                peer = peers.get(digset)

            if peer is not None:
                with peer.lock:
                    if peer.last_messsage_id >= msg['id']:
                        continue

                    if msg['type'] == 'ping':
                        peer.send_msg({'type': 'pong'})
                    elif msg['type'] == 'pong':
                        peer.ping_misses = 0
                    elif msg['type'] == 'chat':
                        print_(f"{peer.digset}::: {msg.get('message')}")

                    peer.last_messsage_id = msg['id']

    # except (ValueError, OSError):
    #     # I/O operation on closed file. (when peer is kicked by monitor)
    #     pass
    except Exception as e:
        print_(
            f"<handle_msg({in_peer.addr})_exc>: {type(e)}>{e}", file=sys.stderr)


def broadcast_msg(msg):
    for peer in peers.values():
        if peer.digset != self_digset:
            with peer.lock:
                peer.send_msg(msg)


def broadcast_ping():
    for addr in broadcast_addrs:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.sendto(b'ping', (addr, cfg.DISCOVERY_PORT))
        s.close()


def init_crypto(peer_socket, host) -> Optional[Peer]:
    peer_socket.settimeout(cfg.TIMEOUT)
    peer_socket_f = peer_socket.makefile(mode='rwb')
    msg = json.dumps({
        'cert': base64.b64encode(cert_b).decode('ascii'),
        'sign': base64.b64encode(cert_sign).decode('ascii')}) + '\n'
    peer_socket.send(msg.encode('utf-8'))

    # the first message should be bas64-encoded certificate
    data = json.loads(peer_socket_f.readline())
    cert = base64.b64decode(data['cert'])
    sign = base64.b64decode(data['sign'])

    try:
        crypto.verify(ca, sign, cert, 'sha512')
        to_verify = secrets.token_bytes(32)

        pubcert = crypto.load_certificate(crypto.FILETYPE_PEM, cert)
        pubkey = pubcert.get_pubkey()
        pubkey_b = crypto.dump_publickey(crypto.FILETYPE_PEM, pubkey)
        pubkey = serialization.load_pem_public_key(pubkey_b)

        msg = enc(to_verify, pubkey) + b'\n'
        peer_socket.send(msg)

        to_dec = dec(peer_socket_f.readline(), key_cr)
        msg_next = enc(to_dec + b'__ok', pubkey) + b'\n'
        peer_socket.send(msg_next)

        resp = dec(peer_socket_f.readline(), key_cr)
        if resp != to_verify + b'__ok':
            raise Exception("Invalid Response")
    except Exception as e:
        msg = json.dumps(
            {'id': 0,
                'type': 'error',
                'message': repr(e)}) + '\n'
        peer_socket.send(msg.encode('utf-8'))
        peer_socket.close()
        return

    pubkey = crypto.load_certificate(crypto.FILETYPE_PEM, cert)
    peer_socket.settimeout(None)
    peer = Peer(host, pubkey, peer_socket, peer_socket_f, current_thread())

    with peer_lock:
        if peer.digset in peers:
            try:
                peer.fsock.close()
                peer.socket.close()
            except Exception:
                pass
            return
        peers[peer.digset] = peer
        peer_addrs.add(peer.addr)
        print_("<new_peer>", peer.digset, file=sys.stderr)

    return peer


def broadcast_receiver():
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            # s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            # s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

            s.bind(('0.0.0.0', cfg.DISCOVERY_PORT))

            while True:
                try:
                    _, addr = s.recvfrom(16)
                    host = addr[0]

                    if host in local_addresses:
                        continue
                    if host in peer_addrs:
                        continue

                    def worker():
                        try:
                            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as peer_socket:
                                peer_socket.settimeout(cfg.TIMEOUT)
                                peer_socket.connect((host, cfg.MESSAGE_PORT))
                                peer = init_crypto(peer_socket, host)
                                if peer:
                                    handle_messages(peer)
                        except Exception as e:
                            print_("<worker>", e)
                            pass

                    t = Thread(target=worker, name=f'worker<{addr}>')
                    t.start()

                except (socket.timeout,
                        binascii.Error,
                        OpenSSL.crypto.Error,
                        OpenSSL.SSL.Error,
                        json.JSONDecodeError,
                        ConnectionRefusedError) as e:
                    print_(
                        f"<broadcast_sock_error>: {type(e)}>{e}", file=sys.stderr)

                time.sleep(.1)
    except Exception:
        traceback.print_exc()
        os._exit(1)


def tcp_socket():
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind(('0.0.0.0', cfg.MESSAGE_PORT))
            s.listen()

            while True:
                t = None
                try:
                    con, addr = s.accept()
                    host = addr[0]

                    with peer_lock:
                        if host in local_addresses:
                            continue
                        if host in peer_addrs:
                            continue

                    def worker():
                        try:
                            with con:
                                peer = init_crypto(con, host)
                                if peer:
                                    handle_messages(peer)
                        except Exception as e:
                            print_("<worker>", e)
                            pass
                    t = Thread(target=worker, name=f'worker<{addr}>')
                    t.start()
                except (socket.timeout,
                        binascii.Error,
                        OpenSSL.crypto.Error,
                        OpenSSL.SSL.Error,
                        json.JSONDecodeError,
                        ConnectionRefusedError) as e:

                    print_(f"<tcp_sock_error>: {type(e)}>{e}", file=sys.stderr)
                    if t:
                        t.join()

                time.sleep(.1)
    except Exception:
        traceback.print_exc()
        os._exit(1)


def monitor():
    try:
        while True:
            with peer_lock:
                for peer in list(peers.values()):
                    with peer.lock:
                        if not peer.chat_worker.is_alive() or peer.ping_misses > cfg.MAX_PING_MISSES or not peer.alive:
                            try:
                                peer.fsock.close()
                                peer.socket.close()
                            except Exception:
                                pass
                            peers.pop(peer.digset)
                            peer_addrs.remove(peer.addr)
                            print_("<disconnected_peer>",
                                   peer.digset, file=sys.stderr)
                            continue

                        peer.send_msg({'type': 'ping'})
                        peer.ping_misses += 1

            broadcast_ping()
            time.sleep(cfg.MONITOR_INTERVAL)
    except Exception:
        traceback.print_exc()
        os._exit(1)


def main():
    broadcast_receiver_t = Thread(target=broadcast_receiver, name='Broadcast')
    msg_t = Thread(target=tcp_socket, name='Message')
    monitor_t = Thread(target=monitor, name='Monitor')
    try:
        broadcast_receiver_t.start()
        msg_t.start()
        monitor_t.start()

        while True:
            print('> ', end='', flush=True)
            l = sys.stdin.readline()
            if l.strip() == '/exit':
                os._exit(0)
            if not l:
                os._exit(0)

            broadcast_msg({'type': 'chat', 'message': l})
    finally:
        broadcast_receiver_t.join()
        msg_t.join()
        monitor_t.join()


if __name__ == '__main__':
    main()
