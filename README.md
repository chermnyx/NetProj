You can install all dependencies via `poetry install` or using pip and requirements.txt


Configuration can be found in cfg.py

### If you are trying to become a peer 

You need to set: 

CA_URL = 'https://networks.project.in.shadowy.space/sign'

Also do not forget to forward DISCOVERY_PORT and MESSAGE_PORT 
You need to create a file named `CA.pem` in folder 'secrets' and put the following key into it:
-----BEGIN CERTIFICATE-----
MIIE+zCCAuOgAwIBAgIUZTJLQIQFnFutT3EICnYt301+tA4wDQYJKoZIhvcNAQEL
BQAwDTELMAkGA1UEBhMCUlUwHhcNMjEwNTE1MTI1ODUyWhcNMjYwNTE0MTI1ODUy
WjANMQswCQYDVQQGEwJSVTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIB
AOEnz4yq3df6UVRFu4det2bJLRoJeT82Th+980K4LS6fdn4TiRg3yK1jLlC1BNj6
+own7HCcQdhjONbf9J0h811kLZuyYCn9hPd9MkOHWvWCq3AI8v8gjoi7tMHc254f
Fw7V/4Qs1iVOrmty7kXElFzM1tUi/rfCYeI/D4Xj7Gj+Yx/CGw2Uu2nC7mB/Hmrf
/1Y8nGsyL3d7x5yZpNmYEFXwAV9k14Ta5sgGbHPpb8x3yPD83NHH00pSiPhYQiQp
0S03N/r6hGQ8niXKBNJ1j0DvhgmSOgPNV9pPscJlgVFwhBcyXIHeQ81TqQlkDz5X
rREh9/Qj6PVdV9vpn7KNSnHf1BvOihU5+zYpqH+lk1Euirw215MJTN/F4gpY+v5K
XdmP3zAfXjqnq0l+gnyTqz7LQxZ70ovfNU9QxjHdbO5m0EMzhaoc1B+QdwZ3+/MD
vgId5dIwHrF3GCAoaYiS2SxbkS3d64L3IfUOw/Mb1u3Xn0hdh6tDi0NUWW1K0Ebt
P8uwnRw5gcKMknTZ8RHRdu0ZnXOfqf+PhnNXNQvP0/9bGYcaw5Om8NQvb1b9HffG
JflO0696fHn6Xo69hcwJDrI+r7OK9lUQ+FRhlrql5Iyz3nnfFUzPhnVT0OjkAoB0
rXgcU4sYTZswNvQPQ39MacCclnPrSCObmw9d3tb3TdDvAgMBAAGjUzBRMB0GA1Ud
DgQWBBTun7rubpRSZuVbhQqp0rzuRA8jYjAfBgNVHSMEGDAWgBTun7rubpRSZuVb
hQqp0rzuRA8jYjAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4ICAQCP
TK9HkWUBgGu2syiopQwYT+XR6HXuaYzELQEKcIvsO1DOYzA+e+NHkv+h0qlFZ3jJ
8ugcZZhmp8gAUvxYT8RLFv6b408c0Y7gcXmypS3vQCo4VKtJ7CPfPvSXQ0QF/pVP
bmVjAl9lZGc94ws53MIBtQ1jOspYlhbe6K5kviUD3kKC2QRqIy3ZQA6vS/KGBBuN
rllt12r6UnRCPUunNJ1QppHg3CEhabTkjld/GCFFcN3VPhgAPCOH1c2bYjb8n6IY
y69RApMdevF+/lIWHhLlnlJ8XbirsrS5BvIrmYY1PsqCiCHn3keZw7wJqDkTgt8N
vjIUh61SSEzRUqZcr+4KIxY46Mad5MWlkfy3P2PZJClwXug4w9WFo0cr+xVPZC3E
k5/2wzcqxVUvJh3GRM4QYHOzylO4xovPd6W4hnkqTgwxnnJsE6Rk8FaR6yoHr1tH
R9x7QF5tPUfVUWr8u1ha3dFo5Ig2fjlypQuBuEFZkZPbrS/beU7HYimctf/QW1rS
AnJhKej7Q3qjxY4L0+2nfUmjj1ihhPotDUPCzp376PtUESFLv4hTCuXAPyJgSBAU
xUbgybS+Xjb2DUsOSakUK+foAPaPimZUQTLWZwYKD+0Ldhaxa2DRgXE/dWn7uPk3
SiCk08GPTCP2oKmkRwU65lSc2oqlU44uhWDFAUENtg==
-----END CERTIFICATE-----

After doing this launch 'chat.py' and you should be fine

### If you want to become CA:

Generate keys using 
```bash
openssl genrsa -des3 -out CA.key 4096
openssl req -x509 -new -nodes -key CA.key -sha256 -days 1825 -out CA.pem
```

Put those keys along with passfraze in cfg.py

This is enough to launch CA.py

Forward needed ports or use your reverse-proxy by nginx or apache

After this you will need to configure https and send link to your CA to all peers 
