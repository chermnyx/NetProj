from os import environ as env
from pathlib import Path
__dirname = Path(__file__).parent / 'secrets'

CA_PRIVATE_KEY = __dirname / 'CA.key'
CA_PASS = env.get('CA_PASS', None)  # FIXME: change it ¯\_(ツ)_/¯
CA_PUBLIC_KEY = __dirname / 'CA.pem'

DISCOVERY_PORT = 7334
MESSAGE_PORT = 7228
CA_URL = 'http://192.168.1.248:7227/sign'
TIMEOUT = 10.
MONITOR_INTERVAL = 10.
MAX_PING_MISSES = 5
